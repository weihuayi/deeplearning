#### 科学计算对机器学习的影响

> [1] Ricky T. Q. Chen, Yulia Rubanova, Jesse Bettencourt, David Duvenaud. "[Neural Ordinary Differential Equations](https://arxiv.org/abs/1806.07366)". Advances in Neural Information Processing Systems. 2018, [code](https://github.com/rtqichen/torchdiffeq/)

We introduce a new family of deep neural network models. Instead of specifying a discrete sequence of hidden layers, we parameterize the derivative of the hidden state using a neural network. The output of the network is computed using a black-box differential equation solver. These continuous-depth models have constant memory cost, adapt their evaluation strategy to each input, and can explicitly trade numerical precision for speed. We demonstrate these properties in continuous-depth residual networks and continuous-time latent variable models. We also construct continuous normalizing flows, a generative model that can train by maximum likelihood, without partitioning or ordering the data dimensions. For training, we show how to scalably backpropagate through any ODE solver, without access to its internal operations. This allows end-to-end training of ODEs within larger models.

> [2] Ruthotto, L., Haber, E. [Deep Neural Networks Motivated by Partial Differential Equations](https://doi.org/10.1007/s10851-019-00903-1). *J Math Imaging Vis* **62,** 352–364 (2019). 



> [3]] J He, J Xu [**MgNet: A Unified Framework of Multigrid and Convolutional Neural Network**](https://link.springer.com/article/10.1007/s11425-019-9547-2). Science China Mathematics, 62, 1331-1354 (2019).



> [4] Ricky T. Q. Chen, David Duvenaud [*Neural Networks with Cheap Differential Operators*](https://arxiv.org/abs/1912.03579)NeurIPS 2019



> [5] Jens Behrmann, Will Grathwohl, Ricky T. Q. Chen [*Invertible Residual Networks*](https://arxiv.org/abs/1811.00995), ICML 2019  [code](https://github.com/jhjacobsen/invertible-resnet)



> [6] Ricky T. Q. Chen, Jens Behrmann, David Duvenaud, Jörn-Henrik Jacobsen [*Residual Flows for Invertible Generative Modeling*](https://arxiv.org/abs/1906.02735)  NeurIPS 2019  [code](https://github.com/rtqichen/residual-flows)