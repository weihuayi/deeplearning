% Encoding: UTF-8

@Article{Greenfeld2019,
  author      = {Daniel Greenfeld and Meirav Galun and Ron Kimmel and Irad Yavneh and Ronen Basri},
  title       = {Learning to Optimize Multigrid PDE Solvers},
  abstract    = {Constructing fast numerical solvers for partial differential equations (PDEs) is crucial for many scientific disciplines. A leading technique for solving large-scale PDEs is using multigrid methods. At the core of a multigrid solver is the prolongation matrix, which relates between different scales of the problem. This matrix is strongly problem-dependent, and its optimal construction is critical to the efficiency of the solver. In practice, however, devising multigrid algorithms for new problems often poses formidable challenges. In this paper we propose a framework for learning multigrid solvers. Our method learns a (single) mapping from a family of parameterized PDEs to prolongation operators. We train a neural network once for the entire class of PDEs, using an efficient and unsupervised loss function. Experiments on a broad class of 2D diffusion problems demonstrate improved convergence rates compared to the widely used Black-Box multigrid scheme, suggesting that our method successfully learned rules for constructing prolongation matrices.},
  date        = {2019-02-25},
  eprint      = {1902.10248v3},
  eprintclass = {math.NA},
  eprinttype  = {arXiv},
  file        = {:http\://arxiv.org/pdf/1902.10248v3:PDF},
  keywords    = {math.NA, cs.LG, cs.NA},
}

@Article{Hsieh2019,
  author      = {Jun-Ting Hsieh and Shengjia Zhao and Stephan Eismann and Lucia Mirabella and Stefano Ermon},
  title       = {Learning Neural PDE Solvers with Convergence Guarantees},
  abstract    = {Partial differential equations (PDEs) are widely used across the physical and computational sciences. Decades of research and engineering went into designing fast iterative solution methods. Existing solvers are general purpose, but may be sub-optimal for specific classes of problems. In contrast to existing hand-crafted solutions, we propose an approach to learn a fast iterative solver tailored to a specific domain. We achieve this goal by learning to modify the updates of an existing solver using a deep neural network. Crucially, our approach is proven to preserve strong correctness and convergence guarantees. After training on a single geometry, our model generalizes to a wide variety of geometries and boundary conditions, and achieves 2-3 times speedup compared to state-of-the-art solvers.},
  date        = {2019-06-04},
  eprint      = {1906.01200v1},
  eprintclass = {cs.NA},
  eprinttype  = {arXiv},
  file        = {:http\://arxiv.org/pdf/1906.01200v1:PDF},
  keywords    = {cs.NA, stat.CO, stat.ML},
}

@Article{Schmitt2019,
  author      = {Jonas Schmitt and Sebastian Kuckuk and Harald Köstler},
  title       = {Optimizing Geometric Multigrid Methods with Evolutionary Computation},
  abstract    = {For many linear and nonlinear systems that arise from the discretization of partial differential equations the construction of an efficient multigrid solver is a challenging task. Here we present a novel approach for the optimization of geometric multigrid methods that is based on evolutionary computation, a generic program optimization technique inspired by the principle of natural evolution. A multigrid solver is represented as a tree of mathematical expressions which we generate based on a tailored grammar. The quality of each solver is evaluated in terms of convergence and compute performance using automated local Fourier analysis (LFA) and roofline performance modeling, respectively. Based on these objectives a multi-objective optimization is performed using strongly typed genetic programming with a non-dominated sorting based selection. To evaluate the model-based prediction and to target concrete applications, scalable implementations of an evolved solver can be automatically generated with the ExaStencils framework. We demonstrate our approach by constructing multigrid solvers for the steady-state heat equation with constant and variable coefficients that consistently perform better than common V- and W-cycles.},
  date        = {2019-10-07},
  eprint      = {1910.02749v2},
  eprintclass = {math.NA},
  eprinttype  = {arXiv},
  file        = {:http\://arxiv.org/pdf/1910.02749v2:PDF},
  keywords    = {math.NA, cs.MS, cs.NA, cs.NE},
}

@Article{Katrutsa2017,
  author      = {Alexandr Katrutsa and Talgat Daulbaev and Ivan Oseledets},
  title       = {Deep Multigrid: learning prolongation and restriction matrices},
  abstract    = {This paper proposes the method to optimize restriction and prolongation operators in the two-grid method. The proposed method is straightforwardly extended to the geometric multigrid method (GMM). GMM is used in solving discretized partial differential equation (PDE) and based on the restriction and prolongation operators. The operators are crucial for fast convergence of GMM, but they are unknown. To find them we propose a reformulation of the two-grid method in terms of a deep neural network with a specific architecture. This architecture is based on the idea that every operation in the two-grid method can be considered as a layer of a deep neural network. The parameters of layers correspond to the restriction and prolongation operators. Therefore, we state an optimization problem with respect to these operators and get optimal ones through backpropagation approach. To illustrate the performance of the proposed approach, we carry out experiments on the discretized Laplace equation, Helmholtz equation and singularly perturbed convection-diffusion equation and demonstrate that proposed approach gives operators, which lead to faster convergence.},
  date        = {2017-11-10},
  eprint      = {1711.03825v1},
  eprintclass = {math.NA},
  eprinttype  = {arXiv},
  file        = {:http\://arxiv.org/pdf/1711.03825v1:PDF},
  keywords    = {math.NA},
}

@Article{Luz2020,
  author      = {Ilay Luz and Meirav Galun and Haggai Maron and Ronen Basri and Irad Yavneh},
  title       = {Learning Algebraic Multigrid Using Graph Neural Networks},
  abstract    = {Efficient numerical solvers for sparse linear systems are crucial in science and engineering. One of the fastest methods for solving large-scale sparse linear systems is algebraic multigrid (AMG). The main challenge in the construction of AMG algorithms is the selection of the prolongation operator -- a problem-dependent sparse matrix which governs the multiscale hierarchy of the solver and is critical to its efficiency. Over many years, numerous methods have been developed for this task, and yet there is no known single right answer except in very special cases. Here we propose a framework for learning AMG prolongation operators for linear systems with sparse symmetric positive (semi-) definite matrices. We train a single graph neural network to learn a mapping from an entire class of such matrices to prolongation operators, using an efficient unsupervised loss function. Experiments on a broad class of problems demonstrate improved convergence rates compared to classical AMG, demonstrating the potential utility of neural networks for developing sparse system solvers.},
  date        = {2020-03-12},
  eprint      = {2003.05744v1},
  eprintclass = {cs.LG},
  eprinttype  = {arXiv},
  file        = {:http\://arxiv.org/pdf/2003.05744v1:PDF},
  keywords    = {cs.LG, stat.ML},
}

@Comment{jabref-meta: databaseType:bibtex;}
