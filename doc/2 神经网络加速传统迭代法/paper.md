## 神经网络加速传统迭代法

> [1] Hsieh, J.-T., Zhao, S., Eismann, S., Mirabella, L., and Ermon, S. [Learning neural pde solvers with convergence guarantees](https://arxiv.org/abs/1906.01200). arXiv preprint arXiv:1906.01200, 2019. [code](https://github.com/francescobardi/pde_solver_deep_learned)

通过神经网络加速传统迭代法（CNN 加速 Jacobi, U-Net 加速 AMG) 

用差分法将(10)离散, 则Poisson方程边值问题转为一代数方程的求解问题
$$
\text {Find } u_{h}: \bar{\Omega}_{h} \rightarrow \mathbb{R} \quad \text { s.t. }\left\{\begin{array}{ll}
\frac{1}{h^{2}}\left(u_{i-1, j}+u_{i+1, j}+u_{i, j-1}+u_{i, j+1}-4 u_{i, j}\right)=f_{i, j} & \text { in } \Omega_{h} \\
u_{i, j}=b_{i, j} & \text { in } \partial \Omega_{h}
\end{array}\right.
$$
用Jacobi迭代法求解, 将求解过程看作计算卷积的过程, 卷积核
$$
\omega_{J}=\left(\begin{array}{ccc}
0 & 1 / 4 & 0 \\
1 / 4 & 0 & 1 / 4 \\
0 & 1 / 4 & 0
\end{array}\right)
$$
定义算子
$$
\underline{\mathcal{G}}(\underline{\boldsymbol{u}}, \underline{\boldsymbol{b}})=\underline{\boldsymbol{G}} \circ \underline{\boldsymbol{u}}+\underline{\boldsymbol{b}}
$$
这里 $o$ 是 Hadamard product,  $\underline{G}, \underline{b} \in \mathbb{R}^{N \times N}$, 使得
$$
\begin{array}{lll}
\underline{G}_{i, j}=1, & \underline{b}_{i, j}=0 & \boldsymbol{x}_{i, j} \in \Omega_{h} \\
\underline{G}_{i, j}=0, & \underline{b}_{i, j}=b\left(\boldsymbol{x}_{i, j}\right) & \boldsymbol{x}_{i, j} \in \partial \Omega_{h}
\end{array}
$$
从而Jacobi 迭代法可以写为
$$
\begin{aligned}
\boldsymbol{u}^{k+1} &=\underline{\Psi}\left(\underline{\boldsymbol{u}}^{k}\right) \\
&=\underline{\mathcal{G}}\left(\omega_{J} * \underline{\boldsymbol{u}}^{k}+\underline{\boldsymbol{f}}, \underline{\boldsymbol{b}}\right) \\
&=\underline{\boldsymbol{G}} \circ\left(\omega_{J} * \underline{\boldsymbol{u}}^{k}+\underline{\boldsymbol{f}}\right)+\underline{\boldsymbol{b}}
\end{aligned}
$$
我们希望找到一个算子$\mathcal{H}$ 来提高收敛速度
$$
\begin{aligned}
\boldsymbol{u}^{k+1} &=\underline{\Phi}_{\mathcal{H}}\left(\boldsymbol{u}^{k}\right) \\
&=\underline{\Psi}\left(\underline{\boldsymbol{u}}^{k}\right)+\mathcal{H}\left(\underline{\Psi}\left(\underline{\boldsymbol{u}}^{k}\right)-\underline{\boldsymbol{u}}^{k}\right)
\end{aligned}
$$
这里
$$
\begin{aligned}
\mathcal{H}(\underline{w}) &=\mathcal{H}_{K \cdots}\left(\mathcal{H}_{3}\left(\mathcal{H}_{2}\left(\mathcal{H}_{1}(\underline{w})\right)\right)\right) \dots \\
\mathcal{H}_{i}(\underline{w}) &=\underline{G} \circ\left(\omega_{i} * \underline{w}\right)
\end{aligned}
$$
通过修改方程的右端项 $f$，边界的几何形状 $G$，边界条件的数值 $b$，网格的大小 n 来构造训练集和测试集

- 训练集：固定的网格大小，固定的方形区域作为求解区域，$f = 0$，边界条件 $b$ 随机生成
- 测试集：包括 4 种不同的测试集：(1) 同样的区域，更密的网格；(2) L 形区域 (3) 圆形区域 (4) 同样的区域，改变右端项 $f$.

目标函数
$$
\min _{\mathcal{H}} \sum_{\underline{G}, \boldsymbol{b}, \boldsymbol{f} \in \mathcal{D} ; k \in \mathcal{D} \mathcal{U}(1,20)}\left\|\underline{\Phi}_{\mathcal{H}}^{k}\left(\underline{\boldsymbol{u}}^{0}, \underline{G}, \underline{\boldsymbol{f}}, \underline{\boldsymbol{b}}\right)-\underline{\boldsymbol{u}}^{\star}(\underline{\boldsymbol{G}}, \underline{\boldsymbol{f}}, \underline{\boldsymbol{b}})\right\|_{2}^{2}
$$
$\underline{\boldsymbol{u}}^{\star}(\underline{\boldsymbol{G}}, \underline{\boldsymbol{f}}, \underline{\boldsymbol{b}})$ 是通过Jacobi 迭代2000次得到的, $k \in \mathcal{D} \mathcal{U}(1,20)$ 表示区间 $[1,20]$ 上的离散均匀分布, $\underline{u}^{0} \sim \mathcal{N}(0,1)$.

**算法：**

<img src="C:\Users\chen\Pictures\image-20200617142517218.png" alt="image-20200617142517218" style="zoom: 33%;" />

> [2] Schmitt, J., Kuckuk, S., and K. ostler, H. [Optimizing geo-metric multigrid methods with evolutionary computation](https://arxiv.org/abs/1910.02749). arXiv preprint arXiv:1910.02749, 2019.

use evolutionary methods to optimize a GMG solver

> [3] Katrutsa, A., Daulbaev, T., and Oseledets, I. [Deep multigrid: learning prolongation and restriction matrices](https://arxiv.org/abs/1711.03825). arXiv preprint arXiv:1711.03825, 2017

optimize restriction and prolongation operators for GMG, by formulating the entire two-grid algorithm as a deep neural network, and approximately minimizing the spectral radius of the resulting iteration matrix

> [4] Greenfeld, D., Galun, M., Basri, R., Yavneh, I., and Kimmel, R. [Learning to optimize multigrid pde solvers](https://arxiv.org/abs/1902.10248). arXiv preprint arXiv:1902.10248, 2019.

uses a multilayer perceptron (MLP) with skip-connections to produce prolongation operators for 2D diffusion partial differential equations discretized on a rectangular grid 

> [5] Ilay Luz, Meirav Galun, Haggai Maron, Ronen Basri, and Irad Yavneh. [Learning algebraic multigrid using graph neural networks](https://arxiv.org/abs/2003.05744), 2020.

Here we propose a framework for learning AMG prolongation operators for linear systems with sparse symmetric positive (semi-) definite matrices. We train a single graph neural network to learn a mapping from an entire class of such matrices to prolongation operators, using an efficient unsupervised loss function. 