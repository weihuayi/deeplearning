> [1] Long Z, Lu Y, Ma X and Dong B (2017), *"[PDE-Net: Learning PDEs from Data](https://arxiv.org/abs/1710.09668)"*, October, 2017.

The main objective of this paper is to accurately predict the dynamics of complex systems and to uncover the underlying hidden PDE models (should they exist) at the same time, with minimal prior knowledge on the systems.





> [2] Long Z, Lu Y and Dong B (2018), *"[PDE-Net 2.0: Learning PDEs from Data with A Numeric-Symbolic Hybrid Deep Network](https://arxiv.org/abs/1812.04426)"*, November, 2018.



> [3] Gulian M, Raissi M, Perdikaris P and Karniadakis G (2019), *"[Machine Learning of Space-Fractional Differential Equations](https://epubs.siam.org/doi/abs/10.1137/18M1204991#citedBySection)"*, SIAM Journal on Scientific Computing., jan, 2019. Vol. 41(4), pp. A2485-A2509. 

