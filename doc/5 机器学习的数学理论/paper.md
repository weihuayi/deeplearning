## 网络的表示能力

> [1] Cybenko G (1989), *"Approximation by superpositions of a sigmoidal function"*, Mathematics of Control, Signals, and Systems., dec, 1989. Vol. 2(4), pp. 303-314. Springer Science and Business Media LLC.
>
> [[BibTeX](javascript:toggleInfo('Cybenko1989','bibtex'))] [[DOI](https://doi.org/10.1007/bf02551274)]

> [2] Hornik K (1991), *"Approximation capabilities of multilayer feedforward networks"*, Neural Networks. Vol. 4(2), pp. 251-257. Elsevier BV.
>
> [[BibTeX](javascript:toggleInfo('Hornik1991','bibtex'))] [[DOI](https://doi.org/10.1016/0893-6080(91)90009-t)]

> [3] Telgarsky M (2015), *"Representation Benefits of Deep Feedforward Networks"*, September, 2015.
>
> [[Abstract](javascript:toggleInfo('Telgarsky2015','abstract'))] [[BibTeX](javascript:toggleInfo('Telgarsky2015','bibtex'))]

> [4] Yarotsky D (2016), *"Error bounds for approximations with deep ReLU networks"*, October, 2016.
>
> [[Abstract](javascript:toggleInfo('Yarotsky2016','abstract'))] [[BibTeX](javascript:toggleInfo('Yarotsky2016','bibtex'))]

> [5] Bölcskei H, Grohs P, Kutyniok G and Petersen P (2017), *"Optimal Approximation with Sparsely Connected Deep Neural Networks"*, May, 2017.
>
> [[Abstract](javascript:toggleInfo('Boelcskei2017','abstract'))] [[BibTeX](javascript:toggleInfo('Boelcskei2017','bibtex'))]

> [6] Daubechies I, DeVore R, Foucart S, Hanin B and Petrova G (2019), *"Nonlinear Approximation and (Deep) ReLU Networks"*, May, 2019.
>
> [[Abstract](javascript:toggleInfo('Daubechies2019','abstract'))] [[BibTeX](javascript:toggleInfo('Daubechies2019','bibtex'))]





## 优化算法

