

# SML用神经网络求解微分方程的解  论文整理 


## 1 早期工作 shallow neural network

### Hopefield  network

> [1]  Lee H and Kang IS (1990), *"Neural algorithm for solving differential equations"*, Journal of Computational Physics., nov, 1990. Vol. 91(1), pp. 110-131. Elsevier BV.
>
> [[BibTeX](javascript:toggleInfo('Lee1990','bibtex'))] [[DOI](https://doi.org/10.1016/0021-9991(90)90007-n)]

### feed-forward  network

> [2]  Meade A and Fernandez A (1994), *"The numerical solution of linear ordinary differential equations by feedforward neural networks"*, Mathematical and Computer Modelling., jun, 1994. Vol. 19(12), pp. 1-25. Elsevier BV.
>
> [[BibTeX](javascript:toggleInfo('Meade1994','bibtex'))] [[DOI](https://doi.org/10.1016/0895-7177(94)90095-7)]

> [3] van Milligen BP, Tribaldos V and Jiménez JA (1995), *"Neural Network Differential Equation and Plasma Equilibrium Solver"*, Physical Review Letters., nov, 1995. Vol. 75(20), pp. 3594-3597. American Physical Society (APS).
>
> [[BibTeX](javascript:toggleInfo('Milligen1995','bibtex'))] [[DOI](https://doi.org/10.1103/physrevlett.75.3594)]

> [4] Lagaris I, Likas A and Fotiadis D (1998), *"Artificial neural networks for solving ordinary and partial differential equations"*, IEEE Transactions on Neural Networks. Vol. 9(5), pp. 987-1000. Institute of Electrical and Electronics Engineers (IEEE).
>
> [[BibTeX](javascript:toggleInfo('Lagaris1998','bibtex'))] [[DOI](https://doi.org/10.1109/72.712178)]

> [5] Lagaris I, Likas A and Papageorgiou D (2000), *"Neural-network methods for boundary value problems with irregular boundaries"*, IEEE Transactions on Neural Networks. Vol. 11(5), pp. 1041-1049. Institute of Electrical and Electronics Engineers (IEEE).
>
> [[BibTeX](javascript:toggleInfo('Lagaris2000','bibtex'))] [[DOI](https://doi.org/10.1109/72.870037)]

### radial basis network

> [6] Jianyu L, Siwei L, Yingjian Q and Yaping H (2002), *"Numerical solution of differential equations by radial basis function neural networks"*, In Proceedings of the 2002 International Joint Conference on Neural Networks. IJCNN'02 (Cat. No.02CH37290). IEEE.
>
> [[BibTeX](javascript:toggleInfo('Jianyu','bibtex'))] [[DOI](https://doi.org/10.1109/ijcnn.2002.1005571)]

### finite element network

> [7] Ramuhalli P, Udpa L and Udpa S (2005), *"Finite-Element Neural Networks for Solving Differential Equations"*, IEEE Transactions on Neural Networks., nov, 2005. Vol. 16(6), pp. 1381-1392. Institute of Electrical and Electronics Engineers (IEEE).
>
> [[BibTeX](javascript:toggleInfo('Ramuhalli2005','bibtex'))] [[DOI](https://doi.org/10.1109/tnn.2005.857945)]

### cellular network

> [8] Chua L and Yang L (1988), *"Cellular neural networks: theory"*, IEEE Transactions on Circuits and Systems., oct, 1988. Vol. 35(10), pp. 1257-1272. Institute of Electrical and Electronics Engineers (IEEE).[[BibTeX](javascript:toggleInfo('Chua1988','bibtex'))] [[DOI](https://doi.org/10.1109/31.7600)]

### wavelet network

> [9] Li X, Ouyang J, Li Q and Ren J (2010), *"Integration Wavelet Neural Network for Steady Convection Dominated Diffusion Problem"*, In 2010 Third International Conference on Information and Computing., jun, 2010. IEEE.
>
> [[BibTeX](javascript:toggleInfo('Li2010','bibtex'))] [[DOI](https://doi.org/10.1109/icic.2010.121)]

## 2 challenging tasks

### turbulence

> [1] Ling J, Kurzawski A and Templeton J (2016), *"Reynolds averaged turbulence modelling using deep neural networks with embedded invariance"*, Journal of Fluid Mechanics., oct, 2016. Vol. 807, pp. 155-166. Cambridge University Press (CUP).
>
> [[BibTeX](javascript:toggleInfo('Ling2016','bibtex'))] [[DOI](https://doi.org/10.1017/jfm.2016.615)]

> [2] Duraisamy K, Iaccarino G and Xiao H (2019), *"Turbulence Modeling in the Age of Data"*, Annual Review of Fluid Mechanics., jan, 2019. Vol. 51(1), pp. 357-377. Annual Reviews.
>
> [[BibTeX](javascript:toggleInfo('Duraisamy2019','bibtex'))] [[DOI](https://doi.org/10.1146/annurev-fluid-010518-040547)]

### Molecular Dynamics & Density functionals

> [3] Zhang L, Han J, Wang H, Car R and Weinan E (2018), *"Deep Potential Molecular Dynamics: A Scalable Model with the Accuracy of Quantum Mechanics"*, Physical Review Letters., apr, 2018. Vol. 120(14) American Physical Society (APS).
>
> [[BibTeX](javascript:toggleInfo('Zhang2018','bibtex'))] [[DOI](https://doi.org/10.1103/physrevlett.120.143001)]

> [4] Li L, Snyder JC, Pelaschier IM, Huang J, Niranjan U-N, Duncan P, Rupp M, Müller K-R and Burke K (2015), *"Understanding machine-learned density functionals"*, International Journal of Quantum Chemistry., nov, 2015. Vol. 116(11), pp. 819-833. Wiley.
>
> [[BibTeX](javascript:toggleInfo('Li2015','bibtex'))] [[DOI](https://doi.org/10.1002/qua.25040)]

### SDE & 高维PDE

> [5] Weinan E, Han J and Jentzen A (2017), *"Deep Learning-Based Numerical Methods for High-Dimensional Parabolic Partial Differential Equations and Backward Stochastic Differential Equations"*, Communications in Mathematics and Statistics., nov, 2017. Vol. 5(4), pp. 349-380. Springer Science and Business Media LLC.
>
> [[BibTeX](javascript:toggleInfo('Weinan2017','bibtex'))] [[DOI](https://doi.org/10.1007/s40304-017-0117-6)]

> [6] Han J, Jentzen A and Weinan E (2018), *"Solving high-dimensional partial differential equations using deep learning"*, Proceedings of the National Academy of Sciences., aug, 2018. Vol. 115(34), pp. 8505-8510. Proceedings of the National Academy of Sciences.
>
> [[BibTeX](javascript:toggleInfo('Han2018','bibtex'))] [[DOI](https://doi.org/10.1073/pnas.1718942115)]

> [7] Raissi M (2018), *"Forward-Backward Stochastic Neural Networks: Deep Learning of High-dimensional Partial Differential Equations"*, April, 2018.
>
> [[DOI](https://arxiv.org/abs/1804.07010)]

> [8] Beck C, Weinan E and Jentzen A (2019), *"Machine Learning Approximation Algorithms for High-Dimensional Fully Nonlinear Partial Differential Equations and Second-order Backward Stochastic Differential Equations"*, Journal of Nonlinear Science., jan, 2019. Vol. 29(4), pp. 1563-1619. Springer Science and Business Media LLC.[[BibTeX](javascript:toggleInfo('Beck2019','bibtex'))] [[DOI](https://doi.org/10.1007/s00332-018-9525-3)]

> [9] Zang Y, Bao G, Ye X and Zhou H (2020), *"Weak adversarial networks for high-dimensional partial differential equations"*, Journal of Computational Physics., jun, 2020. Vol. 411, pp. 109409. Elsevier BV.
>
> [[BibTeX](javascript:toggleInfo('Zang2020','bibtex'))] [[DOI](https://doi.org/10.1016/j.jcp.2020.109409)]

## 3 和传统方法相关联

### Finite Element

> [1] Takeuchi J and Kosugi Y (1994), *"Neural network representation of finite element method"*, Neural Networks., jan, 1994. Vol. 7(2), pp. 389-395. Elsevier BV.
>
> [[BibTeX](javascript:toggleInfo('Takeuchi1994','bibtex'))] [[DOI](https://doi.org/10.1016/0893-6080(94)90031-0)]

> [2] He J and Xu J (2019), *"Relu Deep Neural Networks and Linear Finite Elements"*, Journal of Computational Mathematics., jun, 2020. Vol. 38(3), pp. 502-527. Global Science Press.[[BibTeX](javascript:toggleInfo('2020','bibtex'))] [[DOI](https://doi.org/10.4208/jcm.1901-m2018-0160)]

> [3] Wang M, Cheung SW, Chung ET, Efendiev Y, Leung WT and Wang Y (2018), *"Prediction of Discretization of GMsFEM using Deep Learning"*, October, 2018.
>
> [[DOI](https://arxiv.org/abs/1810.12245)]

### Multigrid & Multiscale

> [4] He J and Xu J (2019), *"MgNet: A unified framework of multigrid and convolutional neural network"*, Science China Mathematics., may, 2019. Vol. 62(7), pp. 1331-1354. Springer Science and Business Media LLC.
>
> [[BibTeX](javascript:toggleInfo('He2019','bibtex'))] [[DOI](https://doi.org/10.1007/s11425-019-9547-2)]

> [5] Fan Y, Lin L, Ying L and Zepeda-Nunez L (2018), *"A multiscale neural network based on hierarchical matrices"*, July, 2018. [[URL](https://arxiv.org/abs/1807.01883)]

### 区域分解

> [6] Li K, Tang K, Wu T and Liao Q (2020), *"D3M: A Deep Domain Decomposition Method for Partial Differential Equations"*, IEEE Access. Vol. 8, pp. 5283-5294. Institute of Electrical and Electronics Engineers (IEEE).
>
> [[BibTeX](javascript:toggleInfo('Li2020','bibtex'))] [[DOI](https://doi.org/10.1109/access.2019.2957200)]

> [7] Grégoire Montavon et al. [Deep Taylor Decomposition of Neural Networks](http://iphome.hhi.de/samek/pdf/MonICML16.pdf) ICML(2016)

## 4 improvement of ANN-based methods

### solution space

> [1] Lagaris I, Likas A and Fotiadis D (1998), *"Artificial neural networks for solving ordinary and partial differential equations"*, IEEE Transactions on Neural Networks. Vol. 9(5), pp. 987-1000. Institute of Electrical and Electronics Engineers (IEEE).
>
> [[BibTeX](javascript:toggleInfo('Lagaris1998','bibtex'))] [[DOI](https://doi.org/10.1109/72.712178)]

> [2] McFall K and Mahan J (2009), *"Artificial Neural Network Method for Solution of Boundary Value Problems With Exact Satisfaction of Arbitrary Boundary Conditions"*, IEEE Transactions on Neural Networks., aug, 2009. Vol. 20(8), pp. 1221-1233. Institute of Electrical and Electronics Engineers (IEEE).
>
> [[BibTeX](javascript:toggleInfo('McFall2009','bibtex'))] [[DOI](https://doi.org/10.1109/tnn.2009.2020735)]

### 网络结构

> [3] Berg J and Nyström K (2018), *"A unified deep artificial neural network approach to partial differential equations in complex geometries"*, Neurocomputing., nov, 2018. Vol. 317, pp. 28-41. Elsevier BV.
>
> [[BibTeX](javascript:toggleInfo('Berg2018','bibtex'))] [[DOI](https://doi.org/10.1016/j.neucom.2018.06.056)]

> [4] Sirignano J and Spiliopoulos K (2018), *"DGM: A deep learning algorithm for solving partial differential equations"*, Journal of Computational Physics., dec, 2018. Vol. 375, pp. 1339-1364. Elsevier BV.
>
> [[BibTeX](javascript:toggleInfo('Sirignano2018','bibtex'))] [[DOI](https://doi.org/10.1016/j.jcp.2018.08.029)]

> [5] Kani JN and Elsheikh AH (2017), *"DR-RNN: A deep residual recurrent neural network for model reduction."*, CoRR. Vol. abs/1709.00939
>
> [[BibTeX](javascript:toggleInfo('journals/corr/abs-1709-00939','bibtex'))] [[URL](https://arxiv.org/abs/1709.00939)]

> [6] Zang Y, Bao G, Ye X and Zhou H (2020), *"Weak adversarial networks for high-dimensional partial differential equations"*, Journal of Computational Physics., jun, 2020. Vol. 411, pp. 109409. Elsevier BV.
>
> [[BibTeX](javascript:toggleInfo('Zang2020','bibtex'))] [[DOI](https://doi.org/10.1016/j.jcp.2020.109409)]

### Loss function

> [7] Weinan E and Yu B (2018), *"The Deep Ritz Method: A Deep Learning-Based Numerical Algorithm for Solving Variational Problems"*, Communications in Mathematics and Statistics., feb, 2018. Vol. 6(1), pp. 1-12. Springer Science and Business Media LLC.
>
> [[BibTeX](javascript:toggleInfo('Weinan2018','bibtex'))] [[DOI](https://doi.org/10.1007/s40304-018-0127-z)]

> [8] Ramuhalli P, Udpa L and Udpa S (2005), *"Finite-Element Neural Networks for Solving Differential Equations"*, IEEE Transactions on Neural Networks., nov, 2005. Vol. 16(6), pp. 1381-1392. Institute of Electrical and Electronics Engineers (IEEE).
>
> [[BibTeX](javascript:toggleInfo('Ramuhalli2005','bibtex'))] [[DOI](https://doi.org/10.1109/tnn.2005.857945)]

> [9] Xu C, Wang C, Ji F and Yuan X (2012), *"Finite-Element Neural Network-Based Solving 3-D Differential Equations in MFL"*, IEEE Transactions on Magnetics., dec, 2012. Vol. 48(12), pp. 4747-4756. Institute of Electrical and Electronics Engineers (IEEE).
>
> [[BibTeX](javascript:toggleInfo('Xu2012','bibtex'))] [[DOI](https://doi.org/10.1109/tmag.2012.2207732)]