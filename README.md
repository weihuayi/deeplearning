# 深度学习与科学计算团队仓库

## introduction

**科学计算(scientific computing)** 专注于通过微分方程模型解决大规模工程、物理问题；**机器学习(machine learning)** 不需要建立物理模型，而是通过构建一个可以学习的黑盒子，取适当的参数就能够拟合任何非线性函数，是一种数据驱动方法。两者都有优缺点：科学计算能够解释数据背后的物理意义，并且能够适应小数据，少参数，但是需要大量的domain knowledge；机器学习不许要有很多的经验，但是需要大数据。两者都有不可代替的优缺点，所以存在一种趋势——将这两个不同领域的方法结合起来，充分发挥两者的优点：既能通过神经网络加速微分方程的求解，也能通过微分方程模型解释神经网络，将这一交叉科学称为**Scientific Machine Learning**.

**深入了解**

- 🔶https://thewinnower.com/papers/25359-the-essential-tools-of-scientific-machine-learning-scientific-ml

- https://www.osti.gov/biblio/1478744-workshop-report-basic-research-needs-scientific-machine-learning-core-technologies-artificial-intelligence

- https://arxiv.org/abs/1907.07587

- https://www.bilibili.com/video/bv1JA411i7vG

## 论文目录

暂时将收集到的论文分为五类, 放在 [doc](https://gitlab.com/weihuayi/deeplearning/-/tree/master/doc) 文件夹:

#### [1 神经网络求解微分方程](https://gitlab.com/weihuayi/deeplearning/-/tree/master/doc/1%20%E7%A5%9E%E7%BB%8F%E7%BD%91%E7%BB%9C%E6%B1%82%E8%A7%A3%E5%BE%AE%E5%88%86%E6%96%B9%E7%A8%8B)

#### [2 神经网络加速传统迭代法](https://gitlab.com/weihuayi/deeplearning/-/tree/master/doc/2%20%E7%A5%9E%E7%BB%8F%E7%BD%91%E7%BB%9C%E5%8A%A0%E9%80%9F%E4%BC%A0%E7%BB%9F%E8%BF%AD%E4%BB%A3%E6%B3%95)

#### [3 神经网络求解反问题](https://gitlab.com/weihuayi/deeplearning/-/tree/master/doc/3%20%E7%A5%9E%E7%BB%8F%E7%BD%91%E7%BB%9C%E6%B1%82%E8%A7%A3%E5%8F%8D%E9%97%AE%E9%A2%98)

#### [4 科学计算对机器学习的影响](https://gitlab.com/weihuayi/deeplearning/-/tree/master/doc/4%20%E7%A7%91%E5%AD%A6%E8%AE%A1%E7%AE%97%E5%AF%B9%E6%9C%BA%E5%99%A8%E5%AD%A6%E4%B9%A0%E7%9A%84%E5%BD%B1%E5%93%8D)

#### [5 机器学习的数学理论](https://gitlab.com/weihuayi/deeplearning/-/tree/master/doc/5%20%E6%9C%BA%E5%99%A8%E5%AD%A6%E4%B9%A0%E7%9A%84%E6%95%B0%E5%AD%A6%E7%90%86%E8%AE%BA)

另外, [**这里**](http://www.cs.colorado.edu/~paco3637/sciml-refs.html)有2019.2之前的论文

**注:**

- 有开源代码的论文直接将代码链接附在论文条目后面, 自己实现的代码放在 [code](https://gitlab.com/weihuayi/deeplearning/-/tree/master/code) 和 [data](https://gitlab.com/weihuayi/deeplearning/-/tree/master/data) 文件夹
- [notes](https://gitlab.com/weihuayi/deeplearning/-/tree/master/notes) 文件夹用于记录每次讨论课的内容
- 有关深度学习的资源, [请参考](https://github.com/ChristosChristofidis/awesome-deep-learning)

#### 免责声明

本仓库上传的内容仅限于群内部学习交流，对流出造成的后果概不负责！

